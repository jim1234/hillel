<form action="" method="POST">
    <input type="text" name="name" placeholder="Type your name..."><br/>
    <input type="password" name="password" placeholder="Type your password..."><br/>
    <input type="email" name="email" placeholder="Type your email..."><br/>
    <textarea name="information" placeholder="Tell something about yourself..."><br/>
    <input type="submit" value="Submit">
</form>

<?php
if(preg_match('/.{5,15}/',$_POST['name'])&&preg_match('/[A-Яa-zА-Яа-я,-_]{8,}/',$_POST['password'])){
    echo $_POST['name'] . '<br/>';
    echo $_POST['password'] . '<br/>';
    echo $_POST['email'] . '<br/>';
    echo preg_replace_callback(
        '/[A-ZА-Я]/',
        function ($match) {
            return mb_strtolower($match[0], 'utf-8');
        },
        $_POST['information']
    );
}
?>