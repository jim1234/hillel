<?php

class User2{
    protected $id;
    protected $password;

    public function __construct($id, $password)
    {
        if(preg_match('/\d/',$id)&&preg_match('/.{8,}/',$password)){
            $this->id = $id;
            $this->password = $password;
        }else{
            throw new Exception("Please, add valid id and password");
        }
    }

    public function getUserData(){
        return $this->id . ' ' . $this->password;
    }
}