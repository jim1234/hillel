<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
require_once('User2.php');

try{
    $user = new User2(10, 'tes');
    echo $user->getUserData();
} catch(Exception $ex){
    echo $ex->getMessage() . ' on line ' . $ex->getLine() . ' ' . $ex->getFile();
}